#!bin/bash
echo "Create directory code"
if ! [ -d "code" ]; then
    mkdir code && mkdir code/my
fi
if ! [ -d "code/my" ]; then
    mkdir code/my
fi
if ! [ -e "code/my/index.php" ]; then
    touch code/my/index.php && echo "<?php phpinfo() ?>">>code/my/index.php
fi
echo "Create directory logs"
if ! [ -d "logs" ]; then
    mkdir logs && mkdir nginx
fi
if ! [ -d "logs/nginx" ]; then
    mkdir logs/nginx
fi
echo "Create directory mysql"
if ! [ -d "mysql" ]; then
    mkdir mysql && mkdir mysql/data
fi
if ! [ -d "mysql/data" ]; then 
    mkdir mysql/data
fi


