# LEMP Development Environment with Docker



### Create By: Andros Serrano Hernández
#### Email: [serrano.hernandez90@gmail.com](mailto:serrano.hernandez90@gmail.com) 

## About

Con esta herramienta simulamos un entorno de desarrollo en [**DOCKER**](http://docker.org) donde desplegamos una servicio [**LEMP**](https://lemp.io/) completo, creando contenedores con [**NGINX**](http://nginx.org/en/), [**PHP**](http://php.net) y [**MYSQL**](http://mysql.com) y siendo el mismo desplegado con DOCKER para su uso por parte de desarrolladores y con la posibilidad de el mismo ser puesto en producción.

También quisimos agregar como una contenedor con [**PHPMYADMIN**](http://www.phpmyadmin.net) para poder gestionar las bases de datos de una manera sencilla por parte de los usuarios, desde una aplicacion web facil y dinamica con dicho proyecto.

## Basic Docker
Docker es un projecto Open Source que nos sirve para desplegar de una manera sencilla contenedores atraves de images pre creadas que se encuentran en [**DOCKER HUB**](https://hub.docker.com), repositorio oficial de imagenes de docker.

Para mas informacion [**DOCKER DOCS**](http://docs.dockre.com).

## How to use this Development Enviroment

Clonamos el repositorio

 ```bash
 git clone <repository url>
 ```

Antes de dar el siguiente paso tenemos que correr el script que nos permite crear los directorios del proyecto que son necesarios para ellos ejecutamos el siguiente comando

```bash
sh create.sh
```

Ya con el repositorio clonado entonces podemos pasar a realizar los siguientes cambios en el fichero `docker-compose.yml`

1. Cambiamos la direccion de la carpeta que esta configurada por defecto a la carpeta donde clonamos el repositorio que tenemos, ya que en este caso esta descrita con la configuracion de la maquina donde se hizo el repositorio. `c:/Users/Andros-PC/Docker` por el directorio local nuestro.
2. En el caso de Windows y MacOs tenemos que compartir la direccion de la torre o carpeta.

Corremos Docker con el siguiente comando

```bash
docker-compose up -d
```

El comando **-d** es para que corra como un demonio en nuestras maquinas para mas infomacion ir a la documentacion oficial

Si ejecutamos el comando `docker ps` veremos que tenemos corrriendo 4 contenedores con las tecnologias antes mencionadas y los puertos de salida de los mismo.

Si queremos eliminar nuestro ambiente de desarrollo y las varibles  en tonces corremos el comando

```bash 
docker-compose down
```

Si queremos saber los contenedores o los nombres de los contenedores que tenemos corriendo en nuestra maquina ejecutamos el siguiente comando:
```bash
docker ps -a
```

Si queremos entrar a uno de los contenedores que esta corriendo podemos ejecutar las siguientes lineas de comando:

```bash
docker exec -it mysql bash
```

## Virtual Host para NGINX para las aplicaciones

### Directorio de nuestros Proyectos PHP
En el repositorio tenemos una carpeta creada con el nombre /code que es donde vamos a colocar los direccotios de los proyectos que estamos trabajando.

### Virtual Host de NGINX para PHP

Para que nuestra aplicacion funcione sin dificultad atravez de una virtual host el mismo tiene que ser creado, para ello tenemos una directorio donde ponemos nuestro virtual host que estamos creando.

```bash
cd /config/nginx/sites-available
```

Dentro del mismo creamos una fichero con el nombre del proyecto de dominio del virtual host que estamos creando y ponemos una configuracion como la siguiente:

```nginx
server{    
    listen 80;
    server_name <Nombre de Dominio>;
    
    root /var/www/code/<Nombre del directorio de la aplicacion>;
    index index.html index.php;

    error_log /var/log/nginx/<Nombre de dominio>.log;
    access_log /var/log/nginx/<Nombre de dominio>.log;

    location / {
        try_files $uri /index.php$is_args$args;
    }    
    
    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php7:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
```
### MYSQL

En el caso de nuestro servidor **MYSQL** el mismo se encuentra saliendo por el puerto por defecto 3306.

El usuario por defecto de MYSQL es `root` y su contraseña `admin` para poder realizar cambios por consola de mysql.

Los cambios de la Base de Datos de almacenan en la direccion `/mysql/data`, esta data queda almacenada y compartida y la misma no se pierde puesto que cada ver que corramos nuestros contenedores estos datos se quedan almacenados y podemos manejarlos de una manera sencilla tanto en desarrollo como en producción.

### PHPMYADMIN

El servidor `phpmyadmin` se encuntra corriendo por el puerto `8100` para su salida en la web y conecta con el servidor `mysql` usando el usuario y la contraseña antes mencionada para poder agregar una base de datos. 

## Other's Details

### Queremos usar como servidor de Base de datos MariaDB y no MYSQL

Para ello nos dirigimos al fichero `docker-compose.yml` y en la seccion del servidor MYSQL que esta definida de la siguiente manera

```yml
mysql:
    container_name: mysql
    image: mysql
    volumes:
      - c:/Users/<Usuario-Local>/Docker/Development/mysql/data:/var/lib/mysql
    environment: 
      MYSQL_ROOT_PASSWORD: admin
```

la cambiamos por la siguiente lineas
```yml
mariadb:
    container_name: mariadb
    image: mariadb
    volumes:
      - c:/Users/<Usuario-Local>/Docker/Development/mysql/data:/var/lib/mysql
    environment: 
      MYSQL_ROOT_PASSWORD: admin
```